#!/usr/bin/python
import sys

'''
Reads cron format string  "* * * * *"
'''
class CronToEnglish(object):
    def __init__(self, minute, hour, day, month, weekday):
        self.minute = minute
        self.hour = hour
        self.day = day
        self.month = month
        self.weekday = weekday

#Parse minute, hour
    def hour_min_to_string(self):
        if self.minute == "*" and self.hour == "*":
            if self.month != "*" or self.day != "*" or self.weekday != "*":
                return "every minute of"
            else:
                return "every minute"
        #check if it is vanilla time
        #if it is then just return the time in h:m format
        elif self.hour.isdigit() and self.minute.isdigit():
            return self.hour.zfill(2) + ":" + self.minute.zfill(2)

        else:
            return self.minute_fragment() + self.hour_fragment()

    def minute_fragment(self):
        if self.minute == '*':
            return "every minute of "

        elif self.minute.isdigit():
            if self.minute == "0" or self.minute == "00" and ',' in self.hour:
                return ""

            if self.minute == "1":
                return self.minute + "st minute of "

            elif self.minute == "2":
                return self.minute + "nd minute of "

            elif self.minute == "3":
                return self.minute + "rd minute of "

            else:
                return self.minute + "th minute of "

        elif '/' in self.minute:
            temp = self.minute.split('/')
            if not temp[-1].isdigit():
                raise ValueError("Invalid input in MINUTE field: "
                                 + self.minute)

            if temp[-1] == "1":
                return "every " + temp[-1] + "st minute of "

            elif temp[-1] == "2":
                return "every " + temp[-1] + "nd minute of "

            elif temp[-1] == "3":
                return "every " + temp[-1] + "rd minute of "

            else:
                return "every " + temp[-1] + "th minute of "

        elif '-' in self.minute:
            temp = self.minute.split('-')
            if not temp[0].isdigit() or not temp[-1].isdigit():
                raise ValueError("Invalid input in MINUTE field: "
                                 + self.minute)
            return "the " + temp[0] + " through " + temp[-1] + " minutes of "

        elif ',' in self.minute:
            temp = self.minute.split(',')
            temp2 = ""
            for arg in temp:
                if not arg.isdigit():
                    raise ValueError("Invalid input in MINUTE field: "
                    + self.minute)

                temp2 += arg
                temp2 += ","
            return temp2[::-1].replace(","[::-1], " minutes of "[::-1], 1)[::-1]

        else:
            raise ValueError("Invalid input in MINUTE field: " + self.minute)

    def hour_fragment(self):
        if self.hour == '*':
            return "every hour"

        elif self.hour.isdigit():
            if self.hour == "1":
                return self.hour + "st hour"

            elif self.hour == "2":
                return self.hour + "nd hour"

            elif self.hour == "3":
                return self.hour + "rd hour"

            else:
                return self.hour + "th hour"

        elif '/' in self.hour:
            temp = self.hour.split('/')
            if not temp[-1].isdigit():
                raise ValueError("Invalid input in MINUTE field: "
                                 + self.minute)

            if temp[-1] == "1":
                return "every " + temp[-1] + "st hour"

            elif temp[-1] == "2":
                return "every " + temp[-1] + "nd hour"

            elif temp[-1] == "3":
                return "every " + temp[-1] + "rd hour"

            else:
                return "every " + temp[-1] + "th hour"

        elif '-' in self.hour:
            temp = self.hour.split('-')
            if not temp[0].isdigit() or not temp[-1].isdigit():
                raise ValueError("Invalid input in HOUR field: " + self.hour)
            return  temp[0] + "-" + temp[-1] + " hour"

        elif ',' in self.hour:
            temp = self.hour.split(',')
            temp2 = ""
            for arg in temp:
                if not arg.isdigit():
                    raise ValueError("Invalid input in HOUR field: "
                    + self.hour)
                temp2 += arg
                temp2 += ","
            return temp2[::-1].replace(","[::-1], "th hour"[::-1], 1)[::-1]

        else:
            raise ValueError("Invalid input in HOUR field: " + self.hour)

#Parse date - day, month, weekday
    def day_month_to_string(self):
        if self.minute == "*" and self.hour == "*" \
            and self.day == '*' and self.month == '*' and self.weekday == '*':
            return ""
        elif self.day == '*' and self.month == '*' and self.weekday == '*':
            return " every day"
        else:
            temp = self.day_fragment()
            temp += self.month_fragment()
            return temp

    def month_fragment(self):
        if self.month == "*":
            if self.day == "*" and self.weekday != "*":
                return ""
            else:
                return " of every month"

        elif self.month.isdigit():
            return " of " + month_name(self.month)

        elif self.month.isalpha():
            return self.month

        elif '/' in self.month:
            temp = self.month.split('/')
            return " of every " + temp[-1] + " months"

        elif '-' in self.month:
            temp = self.month.split('-')
            alpha = all(arg.isalpha() for arg in temp)

            if alpha:
                return " of " + temp[0] + " through " + temp[-1]
            else:
                return " of " + month_name(temp[0]) + " through " \
                + month_name(temp[-1])

        elif ',' in self.month:
            temp = self.month.split(',')
            temp2 = " of "
            alpha = all(arg.isalpha() for arg in temp)

            if alpha:
                for arg in temp:
                    temp2 += arg
                    temp2 += ","
                return temp2[::-1].replace(","[::-1], ""[::-1], 1)[::-1]

            else:
                for arg in temp:
                    if not arg.isdigit():
                        raise ValueError("Invalid input in MONTH field: "
                         + self.month)
                    temp2 += month_name(arg)
                    temp2 += ","
                return temp2[::-1].replace(","[::-1], ""[::-1], 1)[::-1]
        else:
            raise ValueError("Invalid input in MONTH field: " + self.month)

    def day_fragment(self):
        if self.day == "*" and self.weekday == "*":
            return " everyday"

        elif self.day == "*":
            return self.day_of_week_fragment()

        elif self.weekday == "*":
            return self.day_of_month_fragment()

        else:
            return self.day_of_month_fragment() + self.day_of_week_fragment()

    def day_of_month_fragment(self):
        if self.day == "*":
            return "every day"

        elif self.day.isdigit():
            if self.day == "1":
                return " on " + self.day + "st"

            elif self.month == "2":
                return " on " + self.day + "nd"

            elif self.day == "3":
                return " on " + self.day + "rd"

            else:
                return " on " + self.day + "th"

        elif '/' in self.day:
            temp = self.day.split('/')
            return " every " + temp[-1] + "nd day"

        elif '-' in self.day:
            temp = self.day.split('-')
            return " the " + temp[0] + "-" + temp[-1] + "th"

        elif ',' in self.day:
            temp = self.day.split(',')
            temp2 = " on the "
            for arg in temp:
                temp2 += arg
                temp2 += ","

            return temp2[::-1].replace(","[::-1], "th"[::-1], 1)[::-1]

        else:
            raise ValueError("Invalid input in DAY field: " + self.day)

    def day_of_week_fragment(self):
        if self.weekday == "*" or self.weekday == "":
            return ""

        elif self.weekday.isdigit():
            return day_to_string(self.weekday)

        elif '/' in self.weekday:
            temp = self.weekday.split('/')
            return "every " + day_to_string(temp[-1])

        elif '-' in self.weekday:
            temp = self.weekday.split('-')
            alpha = all(arg.isalpha() for arg in temp)

            if alpha:
                if self.day == "*":
                    return  " on every " + temp[0] + "-" + temp[-1]
                else:
                    return  " and on every " + temp[0] + "-" + temp[-1]
            else:
                if self.day == "*":
                    return " on every " + day_to_string(temp[0]) + \
                                    "-" + day_to_string(temp[-1])
                else:
                    return " and on every " + day_to_string(temp[0]) + \
                                        "-" + day_to_string(temp[-1])

        elif ',' in self.weekday:
            temp = self.weekday.split(',')
            temp2 = ""
            if self.day == "*":
                temp2 += " on every "
            else:
                temp2 += " and on every "
            alpha = all(arg.isalpha() for arg in temp)

            if alpha:
                for arg in temp:
                    temp2 += arg
                    temp2 += ","
                return temp2[::-1].replace(","[::-1], ""[::-1], 1)[::-1]

            else:
                for arg in temp:
                    temp2 += day_to_string(arg)
                    temp2 += ","
                return temp2[::-1].replace(","[::-1], ""[::-1], 1)[::-1]

        else:
            raise ValueError("Invalid input in WEEKDAY field: " + self.weekday)
#end of class CronToEnglish


def month_name(month):
    month_dic = {'01': "January", '02': "February", '03': "March",
                 '04': "April", '05': "May", '06': "June",
                 '07': "July", '08': "August", '09': "September",
                 '10': "October", '11': "November", '12': "December"}

    month = month.zfill(2)
    if month_dic.get(month):
        return month_dic.get(month)
    else:
        raise ValueError("Error while parsing MONTH field")


def day_to_string(day_num):
    day_num = day_num.zfill(2)
    weekdays = {'01': "Mon", '02': "Tue", '03': "Wed", '04': "Thur",
                '05': "Fri", '06': 'Sat', '07': 'Sun'}
    if weekdays.get(day_num):
        return weekdays.get(day_num)
    else:
        raise ValueError("Error while parsing DAY field")


def parse_cron(arg_list):
    for arg in arg_list:
        print(arg)
        minute = arg[0] #0-59
        hour = arg[1] #1-12
        day = arg[2]    #1-31
        month = arg[3]  #1-12 Jan, Feb...
        weekday = arg[4] #1-7 Mon, Tue

        eng = CronToEnglish(minute, hour, day, month, weekday)
        print(eng.hour_min_to_string() + eng.day_month_to_string())
        print("--------------\n")


def main():
    try:
        arg_list = []
        for arg in sys.argv[1:]:
            arg_list.append(arg.split())

        parse_cron(arg_list)

    except IndexError:
        print("Invalid Input '* * * * *' format expected\n")
        raise
    except ValueError:
        raise
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise

if __name__ == "__main__":
    main()
